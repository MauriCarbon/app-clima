const chipId = document.getElementById("chipId").value;
document.addEventListener('DOMContentLoaded', () => {
    const botones = document.querySelectorAll(".btn-control");
    show("temperatura");
    botones.forEach(btn=>{
        let result = btn.id.split('-')
        btn.onclick=()=>{
            show(result[1]);
        }
    })
    getUbicacion().then(e=>{
        ubicacion.textContent=e
    })
    cargarDatos()
    setInterval(cargarDatos,60000)
});

function show(name) {
    document.querySelector("#panel-container-fuego").style.display = "none";
    document.querySelector("#panel-container-viento").style.display = "none";
    document.querySelector("#panel-container-humedad").style.display = "none";
    document.querySelector("#panel-container-presion").style.display = "none";
    document.querySelector("#panel-container-temperatura").style.display = "none";
    document.querySelector("#panel-container-"+name).style.display = null;
}

async function queso(){
    const response = await fetch("api2/estacion/loadStation/"+chipId);
    const data = await response.json();
    console.log(data);
    return data;
}

function cargarDatos(){
    queso().then(Station=>{
        let temp = Station.temperatura.split('.')
        let sens = Station.sensacion.split('.')
        document.getElementById("temp-val-int").textContent = temp[0]
        document.getElementById("temp-val-dec").textContent = temp[1]
        document.getElementById("sens-val-int").textContent = sens[0]
        document.getElementById("sens-val-dec").textContent = sens[1]
        document.getElementById("temp-max").textContent = Station.tempmax+"°C"
        document.getElementById("temp-min").textContent = Station.tempmin+"°C"
    })
}

async function getUbicacion(){
    const response = await fetch("api2/estacion/buscar/"+chipId);
    const data = await response.json();
    return data.ubicacion;
}