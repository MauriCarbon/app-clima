<?php
    header("Content-type: application/json");

    header("Access-Control-Allow-Origin:*");
    header("-Credentials:true");
    header("-Methods:PUT,POST,DELETE,GET");

    define("URL_BASE","/alumno/7175/app-clima/api2/");

    $request = array_values(array_filter(explode("/",str_replace(URL_BASE,"",$_SERVER["REQUEST_URI"]))));   
        if(!count($request)){
            $body = array("errno" => 400, "error" => "Faltan parametros");
        }else{
            $model = ucfirst(strtolower($request[0]));    
            if(!file_exists("../models/".$model.".php")){
                $body = array("errno" => 400, "error" => "No existe modelo");
            }else{
                if(!isset($request[1])){
                    $body = array("errno" => 400, "error" => "Faltan parametros");
                }else{
                    include_once "../models/".$model.".php"; 
                    $method = $request[1];
                    if(!method_exists($model,$method)){
                        $body = array("errno" => 400, "error" => "No existe el metodo");
                    }else{
                        $obj = new $model;
                        //request[2] = parametro // falta vefificar si existe la parametro
                        if(isset($request[2])){
                            $parameter1=$request[2];
                        }
                        if(empty($parameter1)){
                            $body = $obj -> $method();
                        }else{
                            if(isset($request[3])){
                            $parameter2 = $request[3];
                            }
                            if(empty($parameter2)){
                            $body = $obj -> $method($parameter1);
                            }else{
                            $body = $obj -> $method($parameter1,$parameter2);
                            }
                        }
                    }
                }
            }
        }        
    echo json_encode($body);
?>