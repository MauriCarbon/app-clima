<?php
    require_once 'Credentials.php';

    class Database{
        public $db_connect;
        public function conectar(){
            $this -> db_connect = mysqli_connect(HOST,USER,PASS,DB,PORT);
            //var_dump($db_connect);
            if($this -> db_connect->errno!=0){
                echo $this -> db_connect->error;
            }else{
                return $this -> db_connect;
            }
        }
        public function consulta($query){
            $this -> conectar();
            $response = $this -> db_connect -> query($query);
            if($this -> db_connect -> errno != 0){
                echo $this -> db_connect -> error;
            }else{
                return $response;
            }
        }
    }

?>