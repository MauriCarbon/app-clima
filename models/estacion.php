<?php
    require_once 'Db.php';

    //$db = new Database();
    //$conn=$db -> conectar();
    //var_dump($db -> consulta ($conn,"SELECT * FROM estaciones;")->fetch_all(MYSQLI_ASSOC));

    class Estacion extends Database{
        public function listar(){
            return $this -> consulta ("SELECT * FROM estaciones;") -> fetch_all (MYSQLI_ASSOC);
        }
        public function buscar(int $chipId){
            $response = $this -> consulta ("SELECT * FROM estaciones WHERE chipId='".$chipId."';");
            if($response->num_rows>0){
                return $response -> fetch_all(MYSQLI_ASSOC)[0];
            }else{
                return false;
            }
        }
        public function buscarLimit(int $chipId,int $limit){
            return $this -> consulta ("SELECT * FROM tiempo WHERE chipId=".$chipId." ORDER BY fecha DESC LIMIT $limit;") -> fetch_all (MYSQLI_ASSOC);
        }
        public function nueva(){
            if($_SERVER['REQUEST_METHOD']=="POST"){
                $datos = $_POST;
                if(!$this->buscar($datos['chipId'])){
                    $response = $this -> consulta("INSERT INTO estaciones(chipId,ubicacion,apodo) VALUES (".$datos['chipId'].",'".$datos['ubicacion']."','".$datos['apodo']."');");
                    return array("errno"=>200,"error"=>"Se ha creado una nueva estacion");
                }else{
                    return array("errno"=>400,"error"=>"No se ha podido crear");
                }
                
            }
        }
        public function loadStation(int $chipId){
            $response = $this -> consulta ("SELECT * FROM tiempo INNER JOIN estaciones ON tiempo.chipId = estaciones.chipId WHERE tiempo.chipId =".$chipId." ORDER BY tiempo.fecha DESC LIMIT 1;");
            if($response->num_rows>0){
                return $response -> fetch_all(MYSQLI_ASSOC)[0];
            }else{
                return false;
            }
        }
    }
    //$estacion = new Estacion();
    //var_dump($estacion -> info(713630,3));
?>